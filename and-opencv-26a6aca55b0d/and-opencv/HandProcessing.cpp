#include "HandProcessing.h"
#include "Mouse.h"

HandProcessing::HandProcessing(){
	id_history=0;
}

HandProcessing::~HandProcessing(){

}

//Recibe un objeto Hand y apartir de ese lo agrega a la historia y decifra el tipo de movimiento.
void HandProcessing::process(Hand mano,int &countIzq,int &countDer,int &counterDouble,int &counterSostenido,bool &isClicked){
	//moverMouse(Punto(mano.center.x*width_screen/width_frame, mano.center.y*high_screen/high_frame));
	//if(mano.isNull()) return; //Si no recibe ninguna mano no hace nada,
	if(id_history==0){ //Si id_history es igual a 0 agrega la mano a la historia.
		history[0]=mano;
		id_history++;
	}else{ //Si no, ent se mueve el mosuse
		Point h1 = history[0].getCenter();
		Point h2 = mano.getCenter();
		vector<Point> myfingers = mano.getFingers();
		if(Global::dist(h1, h2)>3 && (int)myfingers.size()==0) Mouse::moverMouse(h2.x-h1.x, h2.y-h1.y);
		
		//Acciones realizadas por el mouse, segun los diferentes timers (counters)	

		if(countIzq>=TIME_ACTIONS){
			Mouse::clickLeft();
			countIzq=0;
		}

		if(countDer>=TIME_ACTIONS){
			Mouse::clickRight();
			countDer=0;
		}

		if(counterDouble>=TIME_ACTIONS){
			Mouse::doubleClick();
			countDer=0;
		}

		if(counterSostenido>=TIME_ACTIONS){
			Mouse::clickLeftSostenido(isClicked);
			counterSostenido=0;
		}


		id_history=0;
	}
}