#pragma once

#include "Global.h"
#define TIME_ACTIONS 30

class HandProcessing{
public:
	HandProcessing();
	~HandProcessing();

	void process(Hand mano,int &countIzq,int &countDer,int &counterDouble,int &counterSostenido, bool &isClicked);

private:
	int id_history;
	Hand history[3];
};
