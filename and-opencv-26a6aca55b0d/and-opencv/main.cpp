
#include "Global.h"
#include "Mouse.h"
#include "Camera.h"
#include "ImageProcessing.h"
#include "HandProcessing.h"
Mat frame;

int main(int argc, const char **argv){
	Mouse::cambiarCursor(L"images/iron-man-arrow.cur");
	//Se accede a la camara y se configura
	Camera cam = Camera();
  
	//Captura el fondo
	while(true){
		cam.getFrame(frame);
		imshow("bgd", frame);

		if(waitKey(33)>=0) break; //Si una tecla se presiona se sale del loop
	}

	//Elimina ruido del fondo y suaviza la imagen
	blur(frame, frame, Size(3,3));
	destroyWindow("bgd");

	ImageProcessing imageProcessing = ImageProcessing();
	imageProcessing.setBackground(frame);

	HandProcessing handProcessing = HandProcessing();
	//Variables Necesarias para las acciones detectadas, y para la imagen a mostar al usuario
	int countIzq=0,countDer=0,countDouble=0,countSostenido=0;
	bool isClicked=false;
	Mat imagen;
	//Loop principal
	while(true){
		cam.getFrame(frame);
		blur(frame, frame, Size(3,3));//Elimina ruido del fondo y suaviza la imagen
		
		Hand aux = imageProcessing.detectHand(frame);
		aux.draw(frame);
		
		//Guardamos los dedos detectados
		vector<Point> fingersAux = aux.getFingers();

		//Aumenta el contador para hacer click izquierdo si detecta 5 dedos
		if((int)fingersAux.size()==5) countIzq++;
		else countIzq=0;
		
		//Aumenta el contador para hacer click derecho si detecta 3 dedos
		if((int)fingersAux.size()==3)countDer++;
		else countDer=0;

		//Aumenta el contador para hacer double click si detecta 4 dedos
		if((int)fingersAux.size()==4)countDouble++;
		else countDouble=0;
		
		//Aumenta el contador para hacer double click si detecta 2 dedos
		if((int)fingersAux.size()==2)countSostenido++;
		else countSostenido=0;
		
		//Halla la imagen segun los dedos detectados en el frame actual
		imagen = imageProcessing.findInterfaceImage((int)fingersAux.size(),imageProcessing);

		//Muestra la ventana con la imagen segun la cantidad de dedos detectada, y la fija a la parte superior izquierda de la pantalla
		imshow("Dedos",imagen);
		HWND ventana = FindWindow(NULL,L"Dedos");
		SetWindowPos(ventana,HWND_TOPMOST,0,0,0,0,0x0040 | 0x10 |0x1);
		//Muestra la ventana con la imagen de lo que esta detectando y la fija a la parte inferior derecha de la pantalla
		imshow("Deteccion", frame);
		HWND ventana2 = FindWindow(NULL,L"Deteccion");
		SetWindowPos(ventana2,HWND_TOPMOST,Global::width_screen-Global::width_frame,Global::height_screen-(Global::height_frame+50),0,0,0x0040 | 0x10 |0x1);
		//Procesa la Mano
		handProcessing.process(aux,countIzq,countDer,countDouble,countSostenido,isClicked);
		if(waitKey(1)>=0) break;//Si una tecla se presiona se sale del loop
	}

	//Libera Memoria
	frame.release();
	cam.~Camera();
	imageProcessing.~ImageProcessing();
	handProcessing.~HandProcessing();
	Mouse::cambiarCursor(L"C:/Windows/Cursors/aero_arrow.cur");
	destroyAllWindows();
	return 0;
}