#include "Global.h"
#include <Windows.h>

//Se inicializan las variables globales
int Global::width_screen = GetSystemMetrics(SM_CXSCREEN);
int Global::height_screen = GetSystemMetrics(SM_CYSCREEN);
int Global::width_frame = 640;
int Global::height_frame = 480;

double Global::dist(Point p1, Point p2){
	double x = abs(p1.x - p2.x);
	double y = abs(p1.y - p2.y);
	return sqrt(x*x+y*y);
}
//Calcula el angulo de tres puntos
double Global::angle(Point c, Point p1, Point p2){
	return abs(angle(c, p1)-angle(c, p2));
}

//Calcula el angulo de dos puntos con respecto a la horizontal
double Global::angle(Point c, Point p){
 	double dx = c.x-p.x;
	if(dx==0) return PI/2;
	double dy = (-1)*c.y - (-1)*p.y;
	//double h = dist(c, p);
	double ang = atan(dy/dx);
	if(ang>0 || dy>0) return ang;
	else return ang+PI;
}
