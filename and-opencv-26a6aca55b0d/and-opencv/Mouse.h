#ifndef MOUSE_H
#define	MOUSE_H
#define OEMRESOURCE

#include <Windows.h>

#include "Global.h"
#define DELTA 0.9
#define sensibilidad_mouse 3.5

class Mouse {
public:
	static void moverMouse(Point p);
	static void moverMouse(int dx, int dy);
    static void clickLeft();
    static void clickLeft(Point p);
    static void clickRight();
    static void clickRight(Point p);
	static void cambiarCursor(LPCTSTR dir);
	static void clickLeftSostenido(bool &isClicked);
	static void doubleClick();

private:

};

#endif