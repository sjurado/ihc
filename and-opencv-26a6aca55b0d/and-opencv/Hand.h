#ifndef HAND_H
#define	HAND_H

#include "Global.h"

class Hand{
public:
    Hand();
    ~Hand();
    
    Point getCenter();
    void setCenter(Point center);
	vector<Point> getContour();
	void setContour(vector<Point> contours);
    vector<Point> getDefects();
    void setDefects(vector<Point> defects);
    vector<Point> getFingers();
    void setFingers(vector<Point> fingers);
    vector<Point> getHull();
    void setHull(vector<Point> hull);
    RotatedRect getMinRect();
    void setMinRect(RotatedRect minRect);
	
	void draw(Mat &img);

private:
	vector<Point> contour;
	RotatedRect minRect;
	vector<Point> hull;
	vector<Point> fingers;
	vector<Point> defects;
	Point center;
};

#endif