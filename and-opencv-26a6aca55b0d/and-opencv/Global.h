#ifndef GLOBAL_H
#define GLOBAL_H

#include <opencv\cv.h>
#include <opencv\highgui.h>
using namespace cv;

#include <iostream>
using namespace std;

#include "Hand.h"

#define PI 3.1416

class Global{
public:
	static int width_screen;
	static int height_screen;
	static int width_frame;
	static int height_frame;

	static double dist(Point p1, Point p2);
	static double angle(Point c, Point p1, Point p2);
	static double angle(Point c, Point p);

private:

};

#endif