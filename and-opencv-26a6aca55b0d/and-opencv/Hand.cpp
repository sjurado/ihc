#include "Hand.h"
//Se definen los metodos de hand
//Contructor
Hand::Hand(){
    this->contour = vector<Point>();
	this->minRect = RotatedRect();
	this->hull = vector<Point>();
	this->fingers = vector<Point>();
	this->defects = vector<Point>();
	this->center = Point();
}

Point Hand::getCenter(){
	return center;
}

void Hand::setCenter(Point center){
	this->center = center;
}

vector<Point> Hand::getContour(){
	return contour;
}
void Hand::setContour(vector<Point> contour){
	this->contour = contour;
}

vector<Point> Hand::getDefects(){
	return defects;
}

void Hand::setDefects(vector<Point> defects){
	this->defects = defects;
}

vector<Point> Hand::getFingers(){
	return fingers;
}

void Hand::setFingers(vector<Point> fingers){
	this->fingers = fingers;
}

vector<Point> Hand::getHull(){
	return hull;
}

void Hand::setHull(vector<Point> hull){
	this->hull = hull;
}

RotatedRect Hand::getMinRect(){
	return minRect;
}

void Hand::setMinRect(RotatedRect minRect){
	this->minRect = minRect; 
}

void Hand::draw(Mat &img){
	//Se dibuja el minimo rectangulo que contiene el contorno
	Point2f rect_points[4];
	minRect.points(rect_points);

	for(int j=0; j<4; j++)
		line(img, rect_points[j], rect_points[(j+1)%4], Scalar(255,255,255), 1, 8);

	//Se dibuja el contorno
	vector< vector<Point> > contours;
	contours.push_back(contour);
	drawContours(img, contours, 0, Scalar(0,0,255), 2, 8);

	//Se dibujan las convexidades
	for(int j=0; j<(int)hull.size(); j++)
		circle(img, hull[j], 5, Scalar(0,0,255), -1);

	
	for(int i=0; i<(int)defects.size(); i++){
		if(i<(int)fingers.size()-1){
			line( img, fingers[i], defects[i], Scalar(0,255,0), 5 );
			line( img, defects[i], fingers[i+1], Scalar(0,255,0), 5 );
			circle(img, defects[i], 5, Scalar(0,255,255), -1);
			circle(img, fingers[i], 10, Scalar(255,0,0), -1);
			circle(img, fingers[i+1], 10, Scalar(255,0,0), -1);
		}
	}


	//Se dibuja el centro con un radio 20
	circle(img, center, 20, Scalar(0,0,255), -1);
}

//Destructor
Hand::~Hand(){
}
