#pragma once

#include "Global.h"

class ImageProcessing{
public:
	ImageProcessing(void);
	~ImageProcessing(void);
	
	Mat getBackground();
	void setBackground(Mat back);
	Mat fingersImages[5];

	Hand detectHand(Mat &img);
	void removeBackground(Mat &img);
	void skinFilter(Mat &img);
	bool findLargerContour(Mat &img, Hand &mano);
	bool findConvexities(Hand &mano);
	void findCenter(Hand &mano);
	Mat findInterfaceImage(int fingers,ImageProcessing a);

private:
	Mat background;
	Mat frame;
};

