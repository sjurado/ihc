#ifndef CAMERA_H
#define CAMERA_H

#include "Global.h"

class Camera{
public:
	Camera();
	~Camera();

	void getFrame(Mat &frame);

private:
	VideoCapture cap;

};

#endif