#include "Mouse.h"
//Se definen los metods staticos de Mouse

//Mover el mouse al punto deseado en la pantalla
void Mouse::moverMouse(Point p){
	if(p.x<0) p.x=0;
	if(p.y<0) p.y=0;
	if(p.x>=Global::width_screen) p.x = Global::width_screen-1;
	if(p.y>=Global::height_screen) p.y = Global::height_screen-1;

	//Se suaviza el movimiento
	POINT src;
    GetCursorPos(&src);
	double a = (src.y-p.y)*1.0/(src.x-p.x);
	double b = src.y - a*src.x;
	
	while(src.x!=p.x || src.y!=p.y){
		if(src.x==p.x){
			if(src.y<p.y) src.y++;
			else src.y--;
		} else{
			if(src.y==p.y){
				if(src.x<p.x) src.x++;
				else src.x--;
			}else{
				if(src.x<p.x) src.x++;
				else src.x--;
				src.y = (int)(a*src.x + b);
			}
		}
		SetCursorPos(src.x, src.y);
		Sleep((DWORD)DELTA);
	}
}
//Mover el mouse con un desplazamiento relativo
void Mouse::moverMouse(int dx, int dy){
	POINT cursor;
    GetCursorPos(&cursor);
	Point src = Point(cursor.x, cursor.y);
	src.x += sensibilidad_mouse*dx*Global::width_screen/Global::width_frame;
	src.y += sensibilidad_mouse*dy*Global::height_screen/Global::height_frame;
	moverMouse(src);
}
//Click izquierdo
void Mouse::clickLeft(){
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
	mouse_event(MOUSEEVENTF_LEFTUP,   0, 0, 0, GetMessageExtraInfo());
}
//Mover al punto y Click izquierdo
void Mouse::clickLeft(Point p){
	moverMouse(p);
	clickLeft();
}
//Click Derecho
void Mouse::clickRight(){
	mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
	mouse_event(MOUSEEVENTF_RIGHTUP,   0, 0, 0, GetMessageExtraInfo());
}
//Mover al punto y Click Derecho
void Mouse::clickRight(Point p){
	moverMouse(p);
	clickRight();
}
//Cambiar Cursor por el Cursor Definido en la Ruta Dir
void Mouse::cambiarCursor(LPCTSTR dir){
	HINSTANCE hinst;            // handle to current instance  
	HCURSOR hCurs3;
	hCurs3=LoadCursorFromFile(dir);
	SetSystemCursor(hCurs3,OCR_NORMAL); 
}

//CLick Izquierdo Sostenido
void Mouse::clickLeftSostenido(bool &isClicked){
	if(isClicked==false) 
	{
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
		isClicked=true;
	}
	else 
		{
		mouse_event(MOUSEEVENTF_LEFTUP,   0, 0, 0, GetMessageExtraInfo());
		isClicked=false;	
	}
}
//Doble CLick 
void Mouse::doubleClick(){
	UINT doubleClickTime = GetDoubleClickTime();
	SetDoubleClickTime(1);
	clickLeft();
	clickLeft();
	SetDoubleClickTime(doubleClickTime);
}
