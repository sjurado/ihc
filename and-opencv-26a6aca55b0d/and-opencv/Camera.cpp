#include "Camera.h"

Camera::Camera(){
	cap.open(0);
	cap.set(3, Global::width_frame);
	cap.set(4, Global::height_frame);
}

Camera::~Camera(){
	cap.release();
}

void Camera::getFrame(Mat &frame){
	cap >> frame;
	flip(frame, frame, 1);
}
