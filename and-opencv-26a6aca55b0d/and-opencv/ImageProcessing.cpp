#include "ImageProcessing.h"
#define TOLERANCIA 50

ImageProcessing::ImageProcessing() {
	fingersImages[0]=imread("images/0dedos.png",CV_LOAD_IMAGE_COLOR);
	fingersImages[1]=imread("images/2dedos.png",CV_LOAD_IMAGE_COLOR);
	fingersImages[2]=imread("images/3dedos.png",CV_LOAD_IMAGE_COLOR);
	fingersImages[3]=imread("images/4dedos.png",CV_LOAD_IMAGE_COLOR);
	fingersImages[4]=imread("images/5dedos.png",CV_LOAD_IMAGE_COLOR);
}

ImageProcessing::~ImageProcessing() {
	background.release();
	frame.release();
}

Mat ImageProcessing::getBackground() {
	return background;
}


void ImageProcessing::setBackground(Mat back) {
	background = back.clone();
}

Hand ImageProcessing::detectHand(Mat &img) {
	frame = img.clone();
	Hand ret = Hand();

	//Se elimina el fondo
	removeBackground(frame);
	//imshow("removeBackground", frame);

	//Se hace un filtro de piel
	skinFilter(frame);
	//imshow("skinFilter", frame);

	//Se busca el contorno mas grande
	if (!findLargerContour(frame, ret))
		return Hand();

	//Se busca las convexidades
	if (!findConvexities(ret))
		return Hand();

	findCenter(ret);

	return ret;
}

void ImageProcessing::removeBackground(Mat &img) {
	vector<Mat> imgCH(3), backCH(3);
	Mat diferencia;

	//Se divide los canales de color de frame y fondo
	split(img, imgCH);
	split(background, backCH);

	//Se comparan los canales y si la diferencia es mayor a la TOLERANCIA se almacena en diferencia
	diferencia = (abs(imgCH[0] - backCH[0]) > TOLERANCIA)
			| (abs(imgCH[1] - backCH[1]) > TOLERANCIA)
			| (abs(imgCH[2] - backCH[2]) > TOLERANCIA);

	min(imgCH[0], diferencia, imgCH[0]);
	min(imgCH[1], diferencia, imgCH[1]);
	min(imgCH[2], diferencia, imgCH[2]);

	merge(imgCH, img);
}

void ImageProcessing::skinFilter(Mat &img) {
	///Se cambia la imagen a YCC
	cvtColor(img, img, CV_BGR2YCrCb);
	//Se hace un filtro binario
	threshold(img, img, 128, 256, CV_THRESH_BINARY);
	//Se hace un filtro de piel
	inRange(img, Scalar(0, 1, 0), Scalar(0, 255, 255), img);

	//Se erosiona la imagen
	erode(img, img, Mat());
	erode(img, img, Mat());

	//Se dilata
	dilate(img, img, Mat());
	dilate(img, img, Mat());
	dilate(img, img, Mat());
}

bool ImageProcessing::findLargerContour(Mat &img, Hand &mano) {
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	//Se hallan los contornos
	findContours(img, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE,
			Point(0, 0));

	//Si no hay contornos no se hace nada
	if (contours.size() == 0) {
		img = frame;
		return false;
	}

	vector<RotatedRect> minRect(contours.size());

	//Para cada contorno se halla el minimo rectangulo que lo contiene y se selecciona el de mayor area
	int index = 0;
	double max_area = 0;
	for (int i = 0; i < (int) contours.size(); i++) {
		minRect[i] = minAreaRect(Mat(contours[i]));
		Size2f tam = minRect[i].size;
		double area = tam.height * tam.width;
		if (area > max_area) {
			index = i;
			max_area = area;
		}
	}

	//Si el area menor 250 no se hace nada
	if (max_area < 250) {
		return false;
	}

	mano.setContour(contours[index]);
	mano.setMinRect(minRect[index]);
	return true;
}

bool ImageProcessing::findConvexities(Hand &mano) {
	//convexHull
	vector<Point> hull;
	vector<int> hull_I;

	//Se calculan las convexidades

	convexHull(mano.getContour(), hull);
	convexHull(mano.getContour(), hull_I);

	//Si no se encuentran convexidades no se hace nada
	if (hull.size() == 0)
		return false;

	//convexityDefects	
	vector<Vec4i> convexityDef;
	//Se calculan los defectos de las convexidades
	convexityDefects(mano.getContour(), hull_I, convexityDef);

	vector<Point> fingers;
	vector<Point> defects;
	//Se recorren todos los defectos
	for (int i = 0; i < (int) convexityDef.size(); i++) {
		Point ini(mano.getContour()[convexityDef[i][0]]);
		Point end(mano.getContour()[convexityDef[i][1]]);
		Point cen(mano.getContour()[convexityDef[i][2]]);
		float depth = convexityDef[i][3];

		//Si la produndidad del defecto es mayor de 10000,
		//se podria tratar de un dedo.
		if (depth > 10000) {
			//Se calcula el minimo angulo entre las dos 
			double ang = Global::angle(cen, ini, end);
			//Si el angulo es menor a PI*2/3 rad muy seguramente
			//puede tratarse de un dedo.
			if (ang <= (PI * 2 / 3)) {
				fingers.push_back(ini);
				fingers.push_back(end);
				defects.push_back(cen);
			}
		}

	}

vector<Point> fingersReal;
	for(int i=0;i<(int)fingers.size()-1;i++){
		if(Global::dist(fingers[i],fingers[i+1]) > 25){
			fingersReal.push_back(fingers[i]);
		}

	}
	
	if(fingers.size()!=0) fingersReal.push_back(fingers[fingers.size()-1]);
	mano.setHull(hull);
	mano.setFingers(fingersReal);
	mano.setDefects(defects);
	return true;
}

void ImageProcessing::findCenter(Hand &mano) {
	//Moments
	//Calcula los momentos de inercia.
	Moments M = moments(mano.getHull());
	//Se crea un punto en el centro
	Point center = Point(M.m10 / M.m00, M.m01 / M.m00);

	mano.setCenter(center);
}

Mat ImageProcessing::findInterfaceImage(int fingers,ImageProcessing a){
	switch (fingers)
	{
	case 0:
		return a.fingersImages[0];
		break;
	case 2:
		return a.fingersImages[1];
		break;
	case 3:
		return a.fingersImages[2];
		break;
	case 4:
		return a.fingersImages[3];
		break;
	case 5:
		return a.fingersImages[4];
		break;
	default:
		return a.fingersImages[0];
		break;

	}
}